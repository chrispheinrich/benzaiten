open Core.Std
open Re2.Std
module QT = Quiz_t
module QJ = Quiz_j

type flashcard = QT.flashcard =
  { keyword: string
  ; definition: string }
with sexp

type flashcards = QT.flashcards =
  { quiz: string
  ; cards: flashcard array }
with sexp

type regexp_type =
  | Keyword
  | Definition

let keyword= Re2.create_exn "Keyword:\\s*"
let definition= Re2.create_exn "Definition:\\s*"

let rec get_flash_cards ~is_value
    ~str_file ~seen_first_value ~counter ~array_values ~value_type =
  match str_file with
  | [] -> array_values
  | hd::tl ->
    match value_type with
    | Keyword ->
      begin
        if (Re2.matches keyword hd && (not seen_first_value))
        then
          (* First quiz question is matched *)
          let hd = Re2.replace_exn ~f:(fun s -> "") keyword hd in
          let counter = counter + 1 in
          let f = {
            keyword = hd;
            definition = "";
          } in
          get_flash_cards ~is_value:true ~str_file:tl ~seen_first_value:true
            ~counter ~array_values:[|f|] ~value_type
        else if (Re2.matches keyword hd && seen_first_value)
        then
          (* Second+ quiz quiz value is matched *)
          let hd = Re2.replace_exn ~f:(fun s -> "") keyword hd in
          let counter = counter + 1 in
          let f = {
            keyword = hd;
            definition = "";
          } in
          let a = Array.append array_values [|f|] in
          get_flash_cards ~is_value:true ~str_file:tl ~seen_first_value:true
            ~counter ~array_values:a ~value_type
        else if is_value && (not (Re2.matches definition hd))
        then (* Part of a value that extends to multiple lines *)
          let a_idx = counter - 1 in
          let f = array_values.(a_idx) in
          let new_f =
            { keyword = (f.keyword ^ "\n" ^ hd)
            ; definition = f.definition } in
          array_values.(a_idx) <- new_f;
          get_flash_cards ~is_value ~str_file:tl ~seen_first_value
            ~counter ~array_values ~value_type
        else if (Re2.matches definition hd) && is_value
        then
          (* switch to definition *)
          get_flash_cards ~is_value:true ~str_file ~seen_first_value:false
            ~counter ~array_values ~value_type:Definition
        else
          (* not part of a value *)
          get_flash_cards ~is_value:false ~str_file:tl ~seen_first_value
            ~counter ~array_values ~value_type
      end
    | Definition ->
      if (Re2.matches definition hd && (not seen_first_value))
      then
        (* First body solution is matched. Here we grab the correct body_record
           based off of the counter.*)
        let hd = Re2.replace_exn ~f:(fun s -> "") definition hd in
        let a_idx = counter - 1 in
        let f = array_values.(a_idx) in
        let new_f =
          { keyword = f.keyword
          ; definition = hd } in
        array_values.(a_idx) <- new_f;
        get_flash_cards ~is_value:true ~str_file:tl ~seen_first_value:true
          ~counter ~array_values ~value_type
      else if is_value && (not (Re2.matches keyword hd))
      then (* Part of a value that extends to multiple lines *)
        let a_idx = counter - 1 in
        let f = array_values.(a_idx) in
        let new_f =
          { keyword = f.keyword
          ; definition = (f.definition ^ "\n" ^ hd) } in
        array_values.(a_idx) <- new_f;
        get_flash_cards ~is_value ~str_file:tl ~seen_first_value
          ~counter ~array_values ~value_type
      else if (Re2.matches keyword hd) && is_value
      (* switch to definition *)
      then
        get_flash_cards ~is_value:true ~str_file ~seen_first_value:true
          ~counter ~array_values ~value_type:Keyword
      else
        (* not part of a value *)
        get_flash_cards ~is_value:false ~str_file:tl ~seen_first_value
          ~counter ~array_values ~value_type

let re = Re2.create_exn "[\\d+\\.*]*"

let extract_quiz_number txt_file =
  (* Only runs once so no need to pre-compile regex *)
  let open Re in
  let quiz_number = rep (seq [ digit ; rep (char '.') ]) in
  let path = seq [ rep1 any ; char '/' ] in
  let re = seq [ path ; group quiz_number ; str ".txt" ] |> Re.compile in
  let substrings = Re.exec re txt_file in
  Re.get substrings 1

let cltool_json txt_file () =
  let str_file = In_channel.read_lines txt_file in
  let quiz_number = extract_quiz_number txt_file in
  if Re2.matches re quiz_number
  then (* parse str file*)
    let flashcards =
      get_flash_cards ~is_value:false ~str_file ~seen_first_value:false
        ~counter:0 ~array_values:[||] ~value_type:Keyword in
    let cards = { quiz = quiz_number ; cards = flashcards } in
    let json = QJ.string_of_flashcards cards in
    let root = ("./flashcard_json/" ^ quiz_number ) in
    let json_path = Filename.concat root (quiz_number ^ ".json") in
    Unix.mkdir_p root;
    Out_channel.write_all json_path ~data:json
  else failwithf "Incorrect txt file name: %s" txt_file ()
