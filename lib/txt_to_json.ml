open Core.Std
open Quiz
open Re2.Std
module QT = Quiz_t
module QJ = Quiz_j

(** The purpose of this code is to parse a quiz txt-file into a json file.  We
    will first convert from .txt file -> protobuf -> json file. This will allow
    us to quickly create a good representation of the json file using the piqi
    library to do the json heavy lifting. The majority of this parser will
    consist of actually doing step 1. Which is to convert the txt file into the
    protobuf. *)

type regexp = {
  question: Re2.t;
  choice: Re2.t;
  solution: Re2.t;
  answer: Re2.t;
  difficulty: Re2.t;
}

let re2_create_exn = Re2.create_exn ~options:[`Case_sensitive false]

let parse_header header_regexp str =
  if (Re2.matches header_regexp str)
  then
    Re2.replace_exn ~f:(fun x -> "") header_regexp str
  else
    ""

let parse_quiz_header str =
  let quiz_header_regexp = re2_create_exn "Quiz:\\s*" in
  parse_header quiz_header_regexp str

let parse_name_header str =
  let name_header_regexp = re2_create_exn "Name:\\s*" in
  parse_header name_header_regexp str

let parse_pic_header str =
  let pic_header_regexp = re2_create_exn "Picture:\\s*" in
  parse_header pic_header_regexp str

let is_match reg_exp str =
  Re2.matches reg_exp str

type regexp_type =
  | Question
  | Choice
  | Solution
  | Answer
  | Difficulty

let get_regexp (regexp: regexp) (str : regexp_type) =
  (* Gets the regexp that matches str *)
  match str with
  | Question -> regexp.question
  | Choice -> regexp.choice
  | Solution -> regexp.solution
  | Answer -> regexp.answer
  | Difficulty -> regexp.difficulty

let matches_regexp value reg1 reg2 reg3 reg4 =
  (not (is_match reg1 value)) &&
  (not (is_match reg2 value)) &&
  (not (is_match reg3 value)) &&
  (not (is_match reg4 value))

let not_matches_value value type_to_ignore regexp =
  match type_to_ignore with
  | Question -> matches_regexp value regexp.choice regexp.solution regexp.answer regexp.difficulty
  | Choice -> matches_regexp value regexp.question regexp.solution regexp.answer regexp.difficulty
  | Solution -> matches_regexp value regexp.question regexp.choice regexp.answer regexp.difficulty
  | Answer -> matches_regexp value regexp.question regexp.choice regexp.solution regexp.difficulty
  | Difficulty -> matches_regexp value regexp.question regexp.choice regexp.solution regexp.answer


let rec get_quiz_values_stuff
    ~is_value
    ~str_file
    ~seen_first_value
    ~body_counter
    ~choice_counter
    ~array_values
    ~value_type
    ~regexp =
  let value_regexp = get_regexp regexp value_type in
  match str_file with
  | [] -> array_values
  | hd::tl ->
    match value_type with
    | Question ->
      begin
        if (is_match value_regexp hd && (not seen_first_value))
        then
          (* First quiz question is matched *)
          let body_counter = body_counter + 1 in
          let hd = Re2.replace_exn ~f:(fun s -> "") value_regexp hd in
          let b = {
            question = hd;
            choices = [||];
            solution = "";
            answer = "";
            difficulty = "";
          } in
          get_quiz_values_stuff
            ~is_value:true
            ~str_file:tl
            ~seen_first_value:true
            ~body_counter
            ~choice_counter
            ~array_values:[|b|]
            ~value_type
            ~regexp
        else if (is_match value_regexp hd && seen_first_value)
        then
          (* Second+ quiz quiz value is matched *)
          let body_counter = body_counter + 1 in
          let hd = Re2.replace_exn ~f:(fun s -> "") value_regexp hd in
          let b = {
            question = hd;
            choices = [||];
            solution = "";
            answer = "";
            difficulty = "";
          } in
          let a = Array.append array_values [|b|] in
          get_quiz_values_stuff
            ~is_value:true
            ~str_file:tl
            ~seen_first_value:true
            ~body_counter
            ~choice_counter
            ~array_values:a
            ~value_type
            ~regexp
        else if is_value && not_matches_value hd value_type regexp
        then (* Part of a value that extends to multiple lines *)
          let a_idx = body_counter - 1 in
          let b = array_values.(a_idx) in
          let value = b.question in
          let new_b = {
            question = (value ^ "\n" ^ hd);
            choices = b.choices;
            solution = b.solution;
            answer = b.answer;
            difficulty = b.difficulty;
          } in
          array_values.(a_idx) <- new_b;
          get_quiz_values_stuff
            ~is_value
            ~str_file:tl
            ~seen_first_value
            ~body_counter
            ~choice_counter
            ~array_values
            ~value_type
            ~regexp
        else if (is_match regexp.choice hd) && is_value
        (* switch to regexp.choice *)
        then
          get_quiz_values_stuff
            ~is_value:true
            ~str_file
            ~seen_first_value:false
            ~body_counter
            ~choice_counter
            ~array_values
            ~value_type:Choice
            ~regexp:regexp
        else
          (* not part of a value *)
          get_quiz_values_stuff
            ~is_value:false
            ~str_file:tl
            ~seen_first_value
            ~body_counter
            ~choice_counter
            ~array_values
            ~value_type
            ~regexp

      end
    | Choice ->
      begin
        if (is_match value_regexp hd && (not seen_first_value))
        then
          (* First quiz choice is matched. Here we grab the correct body_record
             based off of the body_counter. Since it is the first choice for this body
             we just stick the file-line into a choice array*)
          let a_idx = body_counter - 1 in
          let b = array_values.(a_idx) in
          let hd = Re2.replace_exn ~f:(fun s -> "") value_regexp hd in
          let new_b = {
            question = b.question;
            choices = [|hd|];
            solution = b.solution;
            answer = b.answer;
            difficulty = b.difficulty;
          } in
          let choice_counter = 1 in
          array_values.(a_idx) <- new_b;
          get_quiz_values_stuff
            ~is_value:true
            ~str_file:tl
            ~seen_first_value:true
            ~body_counter
            ~choice_counter
            ~array_values
            ~value_type
            ~regexp
        else if (is_match value_regexp hd && seen_first_value)
        then
          (* Second+ quiz choice is matched. So for each question we have
             multiple choices. Thus, there is an array of choices with its own
             counter. We need to get the correct body record and update its
             choices.
          *)
          let hd = Re2.replace_exn ~f:(fun s -> "") value_regexp hd in
          let a_idx = body_counter - 1 in
          let b = array_values.(a_idx) in
          let c = Array.append b.choices [|hd|] in
          let choice_counter = choice_counter + 1 in
          let new_b = {
            question = b.question;
            choices = c;
            solution = b.solution;
            answer = b.answer;
            difficulty = b.difficulty;            
          } in
          array_values.(a_idx) <- new_b;
          get_quiz_values_stuff
            ~is_value:true
            ~str_file:tl
            ~seen_first_value
            ~body_counter
            ~choice_counter
            ~array_values
            ~value_type
            ~regexp
        else if (is_value && (not_matches_value hd value_type regexp))
        then
          (* A choice that extends multiple lines. Here we need to concatenate the choice onto
             the rest of the choice *)
          let a_idx = body_counter - 1 in
          let b = array_values.(a_idx) in
          let c_idx = choice_counter - 1 in
          let c = b.choices.(c_idx) in
          b.choices.(c_idx) <- (c ^ "\n" ^ hd);
          let new_b = {
            question = b.question;
            choices = b.choices;
            solution = b.solution;
            answer = b.answer;
            difficulty = b.difficulty;
          } in
          array_values.(a_idx) <- new_b;
          get_quiz_values_stuff
              ~is_value
              ~str_file:tl
              ~seen_first_value
              ~body_counter
              ~choice_counter
              ~array_values
              ~value_type
              ~regexp
        else if is_match regexp.question hd
        (* switch to Question *)
        then
          get_quiz_values_stuff
            ~is_value:true
            ~str_file
            ~seen_first_value:true (* seen the first question already *)
            ~body_counter
            ~choice_counter:0
            ~array_values
            ~value_type:Question
            ~regexp
        else if is_match regexp.answer hd
        (* switch to Answer *)
        then
          get_quiz_values_stuff
            ~is_value:true
            ~str_file
            ~seen_first_value:false
            ~body_counter
            ~choice_counter:0
            ~array_values
            ~value_type:Answer
            ~regexp
        else if is_match regexp.solution hd
        (* switch to Solution *)
        then
          get_quiz_values_stuff
            ~is_value:true
            ~str_file
            ~seen_first_value:false
            ~body_counter
            ~choice_counter:0
            ~array_values
            ~value_type:Solution
            ~regexp
        else if is_match regexp.difficulty hd
        (* switch to Difficulty *)
        then
          get_quiz_values_stuff
            ~is_value:true
            ~str_file
            ~seen_first_value:false
            ~body_counter
            ~choice_counter:0
            ~array_values
            ~value_type:Difficulty
            ~regexp            
        else
          (* not part of a value *)
          get_quiz_values_stuff
            ~is_value:false
            ~str_file:tl
            ~seen_first_value
            ~body_counter
            ~choice_counter
            ~array_values
            ~value_type
            ~regexp

      end
    |Solution ->
      begin
        if (is_match value_regexp hd && (not seen_first_value))
        then
          (* First body solution is matched. Here we grab the correct body_record
             based off of the body_counter.*)
          let hd = Re2.replace_exn ~f:(fun s -> "") value_regexp hd in
          let a_idx = body_counter - 1 in
          let b = array_values.(a_idx) in
          let new_b = {
            question = b.question;
            choices = b.choices;
            solution = hd;
            answer = b.answer;
            difficulty = b.difficulty;            
          } in
          array_values.(a_idx) <- new_b;
          get_quiz_values_stuff
            ~is_value:true
            ~str_file:tl
            ~seen_first_value:true
            ~body_counter
            ~choice_counter
            ~array_values
            ~value_type
            ~regexp
        else if (is_value && (not_matches_value hd value_type regexp))
        then
          (* A solution that extends multiple lines. Here we need to concatenate the solution onto
             the rest of the solution *)
          let hd = Re2.replace_exn ~f:(fun s -> "") value_regexp hd in
          let a_idx = body_counter - 1 in
          let b = array_values.(a_idx) in
          let new_b = {
            question = b.question;
            choices = b.choices;
            solution = (b.solution ^ "\n" ^ hd);
            answer = b.answer;
            difficulty = b.difficulty;           
          } in
          array_values.(a_idx) <- new_b;
          get_quiz_values_stuff
              ~is_value
              ~str_file:tl
              ~seen_first_value
              ~body_counter
              ~choice_counter
              ~array_values
              ~value_type
              ~regexp
        else if is_match regexp.question hd
        (* switch to Question *)
        then
          get_quiz_values_stuff
            ~is_value:true
            ~str_file
            ~seen_first_value:true (* seen the first question already *)
            ~body_counter
            ~choice_counter:0
            ~array_values
            ~value_type:Question
            ~regexp
        else if is_match regexp.difficulty hd
        (* switch to Difficulty *)
        then
          get_quiz_values_stuff
            ~is_value:true
            ~str_file
            ~seen_first_value:false
            ~body_counter
            ~choice_counter:0
            ~array_values
            ~value_type:Difficulty
            ~regexp                          
        else if is_match regexp.answer hd
        (* switch to Answer *)
        then
          get_quiz_values_stuff
            ~is_value:true
            ~str_file
            ~seen_first_value:false
            ~body_counter
            ~choice_counter:0
            ~array_values
            ~value_type:Answer
            ~regexp
        else
          (* not part of a value *)
          get_quiz_values_stuff
            ~is_value:false
            ~str_file:tl
            ~seen_first_value
            ~body_counter
            ~choice_counter
            ~array_values
            ~value_type
            ~regexp

      end
    |Answer ->
      begin
        if (is_match value_regexp hd && (not seen_first_value))
        then
          (* First body solution is matched. Here we grab the correct body_record
             based off of the body_counter.*)
          let hd = Re2.replace_exn ~f:(fun s -> "") value_regexp hd in
          let a_idx = body_counter - 1 in
          let b = array_values.(a_idx) in
          let new_b = {
            question = b.question;
            choices = b.choices;
            solution = b.solution;
            answer = hd;
            difficulty = b.difficulty;
          } in
          array_values.(a_idx) <- new_b;
          get_quiz_values_stuff
            ~is_value:true
            ~str_file:tl
            ~seen_first_value:true
            ~body_counter
            ~choice_counter
            ~array_values
            ~value_type
            ~regexp
        else if (is_value && (not_matches_value hd value_type regexp))
        then
          (* A solution that extends multiple lines. Here we need to concatenate the solution onto
             the rest of the solution *)
          let a_idx = body_counter - 1 in
          let b = array_values.(a_idx) in
          let new_b = {
            question = b.question;
            choices = b.choices;
            solution = b.solution;
            answer = (b.answer ^ "\n" ^ hd);
            difficulty = b.difficulty;
          } in
          array_values.(a_idx) <- new_b;
          get_quiz_values_stuff
              ~is_value
              ~str_file:tl
              ~seen_first_value
              ~body_counter
              ~choice_counter
              ~array_values
              ~value_type
              ~regexp
        else if is_match regexp.question hd
        (* switch to Question *)
        then
          get_quiz_values_stuff
            ~is_value:true
            ~str_file
            ~seen_first_value:true (* seen the first question already *)
            ~body_counter
            ~choice_counter:0
            ~array_values
            ~value_type:Question
            ~regexp
        else if is_match regexp.solution hd
        (* switch to Solution *)
        then
          get_quiz_values_stuff
            ~is_value:true
            ~str_file
            ~seen_first_value:false
            ~body_counter
            ~choice_counter:0
            ~array_values
            ~value_type:Solution
            ~regexp
        else if is_match regexp.difficulty hd
        (* switch to Difficulty *)
        then
          get_quiz_values_stuff
            ~is_value:true
            ~str_file
            ~seen_first_value:false
            ~body_counter
            ~choice_counter:0
            ~array_values
            ~value_type:Difficulty
            ~regexp                          
        else
          (* not part of a value *)
          get_quiz_values_stuff
            ~is_value:false
            ~str_file:tl
            ~seen_first_value
            ~body_counter
            ~choice_counter
            ~array_values
            ~value_type
            ~regexp

      end
    |Difficulty ->
      begin
        if (is_match value_regexp hd && (not seen_first_value))
        then
          (* First body solution is matched. Here we grab the correct body_record
             based off of the body_counter.*)
          let hd = Re2.replace_exn ~f:(fun s -> "") value_regexp hd in
          let a_idx = body_counter - 1 in
          let b = array_values.(a_idx) in
          let new_b = {
            question = b.question;
            choices = b.choices;
            solution = b.solution;
            answer = b.answer;
            difficulty = hd;
          } in
          array_values.(a_idx) <- new_b;
          get_quiz_values_stuff
            ~is_value:true
            ~str_file:tl
            ~seen_first_value:true
            ~body_counter
            ~choice_counter
            ~array_values
            ~value_type
            ~regexp
        else if (is_value && (not_matches_value hd value_type regexp))
        then
          (* A solution that extends multiple lines. Here we need to concatenate the solution onto
             the rest of the solution *)
          let a_idx = body_counter - 1 in
          let b = array_values.(a_idx) in
          let new_b = {
            question = b.question;
            choices = b.choices;
            solution = b.solution;
            answer = b.answer;
            difficulty = (b.difficulty ^ "\n" ^ hd);
          } in
          array_values.(a_idx) <- new_b;
          get_quiz_values_stuff
              ~is_value
              ~str_file:tl
              ~seen_first_value
              ~body_counter
              ~choice_counter
              ~array_values
              ~value_type
              ~regexp
        else if is_match regexp.question hd
        (* switch to Question *)
        then
          get_quiz_values_stuff
            ~is_value:true
            ~str_file
            ~seen_first_value:true (* seen the first question already *)
            ~body_counter
            ~choice_counter:0
            ~array_values
            ~value_type:Question
            ~regexp
        else if is_match regexp.solution hd
        (* switch to Solution *)
        then
          get_quiz_values_stuff
            ~is_value:true
            ~str_file
            ~seen_first_value:false
            ~body_counter
            ~choice_counter:0
            ~array_values
            ~value_type:Solution
            ~regexp
        else if is_match regexp.answer hd
        (* switch to Answer *)
        then
          get_quiz_values_stuff
            ~is_value:true
            ~str_file
            ~seen_first_value:false
            ~body_counter
            ~choice_counter:0
            ~array_values
            ~value_type:Answer
            ~regexp
        else
          (* not part of a value *)
          get_quiz_values_stuff
            ~is_value:false
            ~str_file:tl
            ~seen_first_value
            ~body_counter
            ~choice_counter
            ~array_values
            ~value_type
            ~regexp

      end

let cltool_json subject txt_file base_videos_dir is_alternate () =
  let str_file = In_channel.read_lines txt_file in
  (* Parse this file for quiz information *)
  let quiz_number = List.map str_file ~f:parse_quiz_header
		    |> String.concat in
  let quiz_name = List.map str_file ~f:parse_name_header
		    |> String.concat in
  let quiz_has_pictures = List.map str_file ~f:parse_pic_header
		   |> String.concat in

  let regexp = {
    question= re2_create_exn "^\\d{1,2}\\.\\s{1}";
    solution= re2_create_exn "Solution:\\s*";
    answer= re2_create_exn "Answer:\\s*";
    difficulty = re2_create_exn "Difficulty:\\s*";
    choice = re2_create_exn "[a-z]{1}\\.\\s*";
  } in

  let quiz_bodies = get_quiz_values_stuff
      ~is_value:false
      ~str_file
      ~seen_first_value:false
      ~body_counter:0
      ~choice_counter:0
      ~array_values:[||]
      ~value_type:Question
      ~regexp in

  let quiz = {
    number = quiz_number;
    name = quiz_name;
    has_pictures = quiz_has_pictures;
    questions = quiz_bodies;
  } in

  (* Convert from QT.quiz to IOS.t *)
  let ios =
    IOS.of_quiz ~is_alternative:(Option.is_some is_alternate) ~subject quiz in
  let open IOS in
  let json = QJ.string_of_t ios in
  let base = (Int.to_string ios.quiz_no) ^ ". " ^ ios.name in
  let root = ("./json/" ^ base ) in
  let file_path = Filename.concat root (base ^ ".json") in
  Unix.mkdir_p root;
  Out_channel.write_all file_path ~data:json;
  match base_videos_dir with
  | None -> ()
  | Some base_videos ->
    let video_src = Filename.concat base_videos (quiz_number ^ ".mov") in
    let video_dst = Filename.concat root (ios.name ^ ".mov") in
    Unix.rename ~src:video_src ~dst:video_dst

    
