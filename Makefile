include Makefile.inc

define exec_submakes
for dir in lib; do \
	$(MAKE) -C $$dir $@; status=$$?; \
	if [ $$status -ne 0 ]; then exit $$status; fi \
done
endef

all $(TARGETS):
	$(exec_submakes)

clean:
	$(OCAMLBUILD) -clean

.PHONY: clean
