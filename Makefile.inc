.DEFAULT_GOAL := build
TARGETS = deps build install tests unit-tests integration-tests
.PHONY: $(TARGETS)

MAYBE_GO_UP = $(shell if [ ! -d .git ]; then echo ".."; else echo "."; fi)

OCAMLBUILD = cd $(MAYBE_GO_UP) && ocamlbuild -use-ocamlfind -plugin-tag "package(ocamlbuild_atdgen)"